#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <errno.h>

#include <wiringSerial.h>

#include "zfm20.h"

const char bmp_header[] = {0x42, 0x4D, 0x36, 0x24, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x75, 0x00, 0x00, 0x00, 0x28, 0x00,
                           0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x20, 0x01, 0x00, 0x00, 0x01, 0x00, 0x08, 0x00, 0x00, 0x00,
                           0x00, 0x00, 0x00, 0x20, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x10, 0x00,
                           0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x11, 0x11, 0x11, 0x00, 0x22, 0x22,
                           0x22, 0x00, 0x33, 0x33, 0x33, 0x00, 0x44, 0x44, 0x44, 0x00, 0x55, 0x55, 0x55, 0x00, 0x66, 0x66,
                           0x66, 0x00, 0x77, 0x77, 0x77, 0x00, 0x88, 0x88, 0x88, 0x00, 0x99, 0x99, 0x99, 0x00, 0xAA, 0xAA,
                           0xAA, 0x00, 0xBB, 0xBB, 0xBB, 0x00, 0xCC, 0xCC, 0xCC, 0x00, 0xDD, 0xDD, 0xDD, 0x00, 0xEE, 0xEE,
                           0xEE, 0x00, 0xFF, 0xFF, 0xFF};


int begin(int baudrate) {
    return serialOpen("/dev/ttyAMA0", baudrate);
}

void printHeader(short len) {
	printf("     __START__ _______ADDER_______ PKID ___LEN___ ");
	int i, numOfUndescores = (len-2)*4+(len-3)-4;
	for (i = 0; i < numOfUndescores/2; i++) printf("_");
	if (len % 2 == 0)
		printf("_DAT_");
	else
		printf("DATA");
	for (i = 0; i < numOfUndescores/2; i++) printf("_");
	printf(" _CHCKSUM_\n");
}

void writePacket(int fd, int addr, char packettype, short len, char *packet) {
#ifdef FINGERPRINT_DEBUG
	printHeader(len);
    printf("---> 0x%02X", (char)(FINGERPRINT_STARTCODE >> 8));
    printf(" 0x%02X", (char)(FINGERPRINT_STARTCODE));
    printf(" 0x%02X", (char)(addr >> 24));
    printf(" 0x%02X", (char)(addr >> 16));
    printf(" 0x%02X", (char)(addr >> 8));
    printf(" 0x%02X", (char)(addr));
    printf(" 0x%02X", packettype);
    printf(" 0x%02X", (char)(len >> 8));
    printf(" 0x%02X", (char)(len));
#endif

    serialPutchar(fd, (char)(FINGERPRINT_STARTCODE >> 8));
    serialPutchar(fd, (char)(FINGERPRINT_STARTCODE));
    serialPutchar(fd, (char)(addr >> 24));
    serialPutchar(fd, (char)(addr >> 16));
    serialPutchar(fd, (char)(addr >> 8));
    serialPutchar(fd, (char)(addr));
    serialPutchar(fd, packettype);
    serialPutchar(fd, (char)(len >> 8));
    serialPutchar(fd, (char)(len));

    short sum = (len >> 8) + (len&0xFF) + packettype;
    int i;
    for(i = 0; i < len - 2; i++) {
        serialPutchar(fd, packet[i]);
#ifdef FINGERPRINT_DEBUG
        printf(" 0x%02X", packet[i]);
#endif
        sum += packet[i];
    }

#ifdef FINGERPRINT_DEBUG
    printf(" 0x%02X", (char)(sum>>8));
    printf(" 0x%02X", (char)(sum));
    printf("\n");
#endif
    serialPutchar(fd, (char)(sum >> 8));
    serialPutchar(fd, (char)sum);
}

short _getReply(int fd, char packet[], short timeout) {
    char reply[512];
    int idx;
    short timer = 0;

    idx = 0;
#ifdef FINGERPRINT_DEBUG
    printf("<---");
#endif
    while (1) {
        while (serialDataAvail(fd) == -1) {
          usleep(1);
          timer++;
          if (timer >= timeout) return FINGERPRINT_TIMEOUT;
        }
        reply[idx] = serialGetchar(fd);
#ifdef FINGERPRINT_DEBUG
        printf(" 0x%02X", reply[idx]);
#endif
        if ((idx == 0) && (reply[0] != (FINGERPRINT_STARTCODE >> 8)))
        	continue;
        idx++;

        if (idx >= 9) {
        	if ((reply[0] != (FINGERPRINT_STARTCODE >> 8)) ||
        			(reply[1] != (FINGERPRINT_STARTCODE & 0xFF)))
        		return FINGERPRINT_BADPACKET;
        	char packettype = reply[6];
        	short len = reply[7];
        	len <<= 8;
        	len |= reply[8];
        	len -= 2;
        	if (idx <= (len+10)) { continue;  }
        	packet[0] = packettype;
        	short i;
        	for (i = 0; i < len; i++) {
        		packet[1 + i] = reply[9 + i];
        	}
#ifdef FINGERPRINT_DEBUG
            printf("\n");
#endif
        	return len;
        }
    }
}

short getReply(int fd, char packet[]) {
    return _getReply(fd, packet, DEFAULTTIMEOUT);
}

void writeImageBufferToFile (int fd, char* filename) {
    FILE* f = fopen(filename, "w");
    if (f == NULL) {
        printf("Error opening %s: %m\n", filename);
        exit(errno);
    }
    fwrite(bmp_header, 1, sizeof(bmp_header), f);
    short x, y, i = 0;
    short len = 0;
    char packet[512];
    char image_data[256*288];
    for (y = 287; y >= 0; y--) {
        for (x = 0; x < 256;) {
            if (i== len) {
                len = getReply(fd, packet);
                i = 0;
            }
            image_data[y * 256 + x++] = packet[i+1]>>4;
            image_data[y * 256 + x++] = packet[i+1]&0x0F;
            i++;
        }
    }
    fwrite(image_data, 1, sizeof(image_data), f);
    fclose(f);
}

void writeCharBufferToFile(int fd, char* filename) {
    FILE *f = fopen(filename, "w");
    if (f == NULL) {
        printf("Error opening %s: %m\n", filename);
        exit(errno);
    }
    int i;
    char receivedPacketType;
    char packet[512];
    while (receivedPacketType != FINGERPRINT_ENDDATAPACKET) {
          short len = getReply(fd, packet);
          receivedPacketType = packet[0];
          for (i = 0; i < len; i++) fputc(packet[i+1], f);
    }
    fclose(f);
}

_Bool verifyPassword(int fd) {
    int password = 0;
    char packet[] = {FINGERPRINT_VERIFYPASSWORD,
                      (password >> 24), (password >> 16),
                      (password >> 8), password};
    writePacket(fd, 0xFFFFFFFF, FINGERPRINT_COMMANDPACKET, 7, packet);
    char len = getReply(fd, packet);

    if ((len == 1) && (packet[0] == FINGERPRINT_ACKPACKET) && (packet[1] == FINGERPRINT_OK))
      return 1;
    return 0;
}

char getImage(int fd) {
  char packet[] = {FINGERPRINT_GETIMAGE};
  writePacket(fd, 0xFFFFFFFF, FINGERPRINT_COMMANDPACKET, 3, packet);
  char len = getReply(fd, packet);

  char receivedPacketType = packet[0];
  if ((len != 1) && (receivedPacketType != FINGERPRINT_ACKPACKET))
   return -1;
  return packet[1];
}

char image2Tz(int fd, char slot) {
  char packet[] = {FINGERPRINT_IMAGE2TZ, slot};
  writePacket(fd, 0xFFFFFFFF, FINGERPRINT_COMMANDPACKET, sizeof(packet)+2, packet);
  char len = getReply(fd, packet);

  if ((len != 1) && (packet[0] != FINGERPRINT_ACKPACKET))
   return -1;
  return packet[1];
}

//transfer a fingerprint template from Char Buffer 1 to host computer
void getModel(int fd, char slot, char* filename) {
    char packet[] = {FINGERPRINT_UPLOADTEMPLATE, slot};
    writePacket(fd, 0xFFFFFFFF, FINGERPRINT_COMMANDPACKET, sizeof(packet)+2, packet);
    char len = getReply(fd, packet);

    if ((len != 1) && (packet[0] != FINGERPRINT_ACKPACKET))
        return;
    writeCharBufferToFile(fd, filename);
}

char createModel(int fd) {
  char packet[] = {FINGERPRINT_REGMODEL};
  writePacket(fd, 0xFFFFFFFF, FINGERPRINT_COMMANDPACKET, sizeof(packet)+2, packet);
  char len = getReply(fd, packet);

  if ((len != 1) && (packet[0] != FINGERPRINT_ACKPACKET))
   return -1;
  return packet[1];
}

void getImageBuffer(int fd, char* filename) {
    char packet[512] = {FINGERPRINT_UPLOADIMAGE};
    writePacket(fd, 0xFFFFFFFF, FINGERPRINT_COMMANDPACKET, 3, packet);

    getReply(fd, packet);

    char receivedPacketType = packet[0];
    if (receivedPacketType != FINGERPRINT_ACKPACKET)
        return;

    writeImageBufferToFile(fd, filename);
}

char setBaudRate(int fd, int baudrate) {
	char n = baudrate / 9600;
	if (n < 1 || n > 12)
		return -1;
	char packet[] = {FINGERPRINT_SETPARAMETER, 4, n};
	    writePacket(fd, 0xFFFFFFFF, FINGERPRINT_COMMANDPACKET, sizeof(packet)+2, packet);
	    char len = getReply(fd, packet);

	    if ((len != 1) && (packet[0] != FINGERPRINT_ACKPACKET))
	        return -1;
	    return packet[1];
}

char setDataPackageLength(int fd, int length) {
	char n = log(length/32)/log(2);
	if (n < 0 || n > 3)
		return -1;
	char packet[] = {FINGERPRINT_SETPARAMETER, 6, n};
	    writePacket(fd, 0xFFFFFFFF, FINGERPRINT_COMMANDPACKET, sizeof(packet)+2, packet);
	    char len = getReply(fd, packet);

	    if ((len != 1) && (packet[0] != FINGERPRINT_ACKPACKET))
	        return -1;
	    return packet[1];
}
