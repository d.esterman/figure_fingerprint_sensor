/*
 * paillier_diff.c
 *
 *  Created on: 29.12.2015
 *      Author: DannyLo
 */

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <limits.h>
#include <math.h>
#include <gmp.h>
#include <paillier.h>

#define square(x) (x*x)

#define MAX_DISTANCE 5625

paillier_ciphertext_t* calc_diff(paillier_pubkey_t* pub,
		paillier_ciphertext_t* res,
		paillier_ciphertext_t* subj_dist,
		int file_dist);

paillier_ciphertext_t* calc_squared_diff(paillier_pubkey_t* pub,
		paillier_ciphertext_t* res,
		paillier_ciphertext_t* subj_dist,
		paillier_ciphertext_t* subj_dist_squared,
		int file_dist);

int intlen(int i);

void exit_if_error(char* str, int val, char* endptr);

int intlen(int i) {
	return (int)(i < 0 ? floor(log(-i)/log(10) + 2) : i == 0 ? 1 : floor(log(i)/log(10)) + 1);
}

void exit_if_error(char* str, int val, char* endptr)
{
	if ((errno == ERANGE && (val == LONG_MAX || val == LONG_MIN))
                   || (errno != 0 && val == 0)) {
		fprintf(stderr, "Error converting %s, %m\n", str);
		exit(EXIT_FAILURE);
	}
	if (endptr == str) {
		fprintf(stderr, "No digits were found in %s\n", str);
		exit(EXIT_FAILURE);
	}		
}

int main(int argc, char **argv)
	{
	paillier_pubkey_t* pk;
	paillier_prvkey_t* sk;
	
	long d1, d2, dz, ind;
	
	paillier_plaintext_t* pt;
	paillier_ciphertext_t* ct;
	paillier_ciphertext_t* ct_sq;

	char* endptr;
	
	if (argc < 3) {
		fprintf(stderr, "Usage: paillier_diff a b\n"
		        "       (a and b are integers)\n"
				"Calculates abs(a-b) using homomorphic properties of paillier cryptosystem\n");
		exit(EXIT_FAILURE);
	}

	d1 = strtol(argv[1], &endptr, 10);
	exit_if_error(argv[1], d1, endptr);
	d2 = strtol(argv[2], &endptr, 10);
	exit_if_error(argv[2], d2, endptr);
	
	// generate keys
	paillier_keygen(1024, &pk, &sk, paillier_get_rand_devurandom);

	pt = paillier_plaintext_from_si(d1);
	ct = paillier_enc(NULL, pk, pt, paillier_get_rand_devurandom);
	calc_diff(pk, ct, ct, d2);
	paillier_dec(pt, pk, sk, ct);
	if (mpz_cmp_ui(pt->m, MAX_DISTANCE) > 0) {
		mpz_sub(pt->m, pt->m, pk->n);
	}
	dz = mpz_get_ui(pt->m);

	printf("Homomorphic calculation of abs(%d - %d): d(c(%d) * c(%d)^-1) = %d\n",
					       d1,  d2,      d1,     d2,       dz);

	mpz_set_si(pt->m, d1);
	paillier_enc(ct, pk, pt, paillier_get_rand_devurandom);
	mpz_set_ui(pt->m, square(d1));
	ct_sq = paillier_enc(NULL, pk, pt, paillier_get_rand_devurandom);

	calc_squared_diff(pk, ct, ct, ct_sq, d2);
	paillier_dec(pt, pk, sk, ct);
	dz = mpz_get_ui(pt->m);
	ind = 36+intlen(d1)+intlen(d2);
	printf("\n");
	printf("Homomorphic calculation of abs(%d - %d) = sqrt((%d - %d)^2) =\n"
		"%*s= sqrt(d(c(%d^2) * c(%d)^(-2*%d) * c(%d^2))) = %d\n",
                                               d1,  d2,         d1,  d2,
		 ind, "",      d1,       d1,     d2,     d2, (int)sqrt(dz));

        paillier_freeplaintext(pt);
        paillier_freeciphertext(ct);
        paillier_freeciphertext(ct_sq);
        paillier_freepubkey(pk);
        paillier_freeprvkey(sk);

	return EXIT_SUCCESS;
}

paillier_ciphertext_t* calc_diff(paillier_pubkey_t* pub,
		paillier_ciphertext_t* res,
		paillier_ciphertext_t* subj_dist,
		int file_dist)
	{
	paillier_plaintext_t* pt;
	paillier_ciphertext_t* ct;
	
	pt = paillier_plaintext_from_si(file_dist);
	if(!res) {
		res = paillier_create_enc_zero();
	}
	ct = paillier_enc(NULL, pub, pt, paillier_get_rand_devurandom);
	mpz_invert(ct->c, ct->c, pub->n_squared);
	paillier_mul(pub, res, ct, subj_dist);
	paillier_freeplaintext(pt);
	paillier_freeciphertext(ct);
	return res;
}

paillier_ciphertext_t* calc_squared_diff(paillier_pubkey_t* pub,
		paillier_ciphertext_t* res,
		paillier_ciphertext_t* subj_dist,
		paillier_ciphertext_t* subj_dist_squared,
		int file_dist) {
	if (file_dist == 0) {
		return subj_dist_squared;
	}
	paillier_plaintext_t* pt;
	paillier_ciphertext_t* ct;

	pt = paillier_plaintext_from_ui(2*abs(file_dist));
	
        if(!res) {
                res = paillier_create_enc_zero();
        }
	paillier_exp(pub, res, subj_dist, pt);
	if (file_dist >= 0) {
		mpz_invert(res->c, res->c, pub->n_squared);
	}
	mpz_set_ui(pt->m, square(file_dist));
	ct = paillier_enc(NULL, pub, pt, paillier_get_rand_devurandom);
        
	paillier_mul(pub, res, res, subj_dist_squared);
	paillier_mul(pub, res, res, ct);
	paillier_freeplaintext(pt);
	paillier_freeciphertext(ct);
	return res;
}

