#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

#include <wiringSerial.h>
#include "../zfm20/zfm20.h"

int main(int argc, char **argv) {
	if (argc < 2) {
			printf("Usage: flush <baud_rate>\n");
			return 1;
	}
	int baudRate = atoi(argv[1]);

    int fd;

    if ((fd  = begin(baudRate)) < 0) {
        fprintf(stderr, "Unable to open serial device: %s\n", strerror(errno));
        return 1;
    }

    serialFlush(fd);
    return 0;
}
