#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

#include "../zfm20/zfm20.h"

int main(int argc, char **argv) {
	if (argc < 3) {
		printf("Usage: setBaudRate <current_baud_rate> <new_baud_rate>\n");
		return 1;
	}
	int currentBaudRate = atoi(argv[1]);
	int newBaudRate = atoi(argv[2]);

    int fd;

    if ((fd  = begin(currentBaudRate)) < 0) {
        fprintf(stderr, "Unable to open serial device: %s\n", strerror(errno));
        return 1;
    }

    if (verifyPassword(fd))
    	printf("Found fingerprint sensor!\n");
    else {
    	printf("Could not find the fingerprint sensor :(\n");
    	return 1;
    }

    printf("Setting baudrate to %d\n", newBaudRate);
    char p = setBaudRate(fd, newBaudRate);
    switch (p) {
         case FINGERPRINT_OK:
           printf("Baud rate set to %d\n", newBaudRate);
           break;
         case FINGERPRINT_PACKETRECIEVEERR:
           printf("Communication error\n");
           return 1;
         case FINGERPRINT_INVALIDREG:
           printf(" Invalid register number\n");
           return 1;
         default:
           printf("Unknown error\n");
           return 1;
    }
	return 0;
}
